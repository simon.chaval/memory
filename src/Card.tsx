import React from 'react';
import PropTypes, { InferProps } from 'prop-types';
import './Card.css';

/**
 * Hidden symbol.
 */
const HIDDEN_SYMBOL: string = '❓';

/**
 * Function to display a card.
 */
export function Card({ card, index, feedback, onClick }: InferProps<typeof Card.propTypes>) {
    return (
        <div className={`card ${feedback}`} onClick={() => onClick(index)}>
            <span className='symbol'>
                {feedback === 'hidden' ? HIDDEN_SYMBOL : card}
            </span>
        </div>
    );
}

Card.propTypes = {
    /**
     * Symbol of the card.
     */
    card: PropTypes.string.isRequired,
    /**
     * Feedback of the card.
     */
    feedback: PropTypes.oneOf([
        'hidden',
        'justMatched',
        'justMismatched',
        'visible',
    ]).isRequired,
    /**
     * Identifier of the card.
     */
    index: PropTypes.number.isRequired,
    /**
     * Gets called when the user clicks on a card.
     */
    onClick: PropTypes.func.isRequired,
}