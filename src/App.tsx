import React, { Component } from 'react';
import './App.css';
import { Card } from './Card';
import { GuessCount } from './GuessCount';

/**
 * All symbols for the memory game.
 */
const SYMBOLS: string[] = ['🐶', '🐱', '🐭', '🐰', '🦊', '🐻', '🐼', '🐨', '🐯', '🐮', '🐷', '🐸', '🐵', '🐴', '🦋', '🐞', '🐌', '🐥'];

/**
 * Card visibility time in milliseconds when they are turned over.
 */
const VISUAL_PAUSE_MSECS = 750;

/**
 * Maximum error in one game.
 */
const MAX_ERRORS: number = 40;

/**
 * Enumaration, representing the state of the game.
 */
enum GameState {
  WIN,
  LOOSE,
  IN_PROGRESS,
}

/**
 * Main component manage the memory game.
 */
class App extends Component {

  /**
   * Global variable contains all parameters of memory game.
   */
  state = {
    cards: Array<string>(),
    currentPair: Array<number>(),
    guesses: 0,
    remainingTry: MAX_ERRORS,
    matchedCardIndices: Array<number>()
  }

  /**
   * Before the component mounts, initialize our state.
   */
  componentWillMount() {
    this.setState({ cards: this.generateCards() });
  }

  /**
   * Shuflle an array of string.
   *
   * @param array array that will be shuffled.
   * @return array shuffled.
   */
  shuffle(array: string[]): string[] {
    return [...array].sort(() => Math.random() - 0.5);
  }

  /**
   * Randomly generate game cards.
   *
   * @return array of cards randomly generated.
   */
  generateCards(): string[] {
    const result: string[] = [];
    const candidates: string[] = this.shuffle(SYMBOLS);

    while (candidates.length > 0) {
      var card = candidates.pop();
      if (card !== undefined)
        result.push(card, card);
    }

    return this.shuffle(result);
  }

  /**
   * Allow you to know the card status.
   *
   * @param index card identifier.
   * @return the status of the card : "visible, hidden, justMatched, justMismatched".
   */
  getFeedbackForCard(index: number): string {
    const { currentPair, matchedCardIndices } = this.state;
    const indexMatched = matchedCardIndices.includes(index);

    if (currentPair.length < 2)
      return indexMatched || index === currentPair[0] ? 'visible' : 'hidden';

    if (currentPair.includes(index))
      return indexMatched ? 'justMatched' : 'justMismatched';

    return indexMatched ? 'visible' : 'hidden';
  }

  /**
   * Manage click on card and current pair.
   *
   * @param index card identifier.
   */
  handleCardClick = (index: number) => {
    const { currentPair } = this.state;

    if (this.gameState() !== GameState.IN_PROGRESS)
      return;

    if (currentPair[0] === index)
      return;

    if (currentPair.length === 0) {
      this.setState({ currentPair: [index] });
      return;
    }

    if (currentPair.length === 1) {
      this.state.currentPair.push(index);
      this.handleNewPairClosedBy();
      return;
    }
  }

  /**
   * Check if the pair matched or not.
   */
  handleNewPairClosedBy() {
    const { cards, currentPair, guesses, matchedCardIndices } = this.state;
    const newGuesses = guesses + 1;
    const matched = cards[currentPair[0]] === cards[currentPair[1]];
    this.setState({ currentPair: currentPair, guesses: newGuesses });
    if (matched)
      this.setState({ matchedCardIndices: [...matchedCardIndices, ...currentPair] });
    else
      this.setState({ remainingTry: this.state.remainingTry - 1 });
    setTimeout(() => this.setState({ currentPair: [] }), VISUAL_PAUSE_MSECS);
  }

  /**
   * Indicate state of the game.
   *
   * @return game state enumerate.
   */
  gameState() {
    const { cards, remainingTry, matchedCardIndices } = this.state;
    if (matchedCardIndices.length === cards.length)
      return GameState.WIN;
    else if (remainingTry <= 0)
      return GameState.LOOSE;
    return GameState.IN_PROGRESS;
  }

  /**
   * Reset the game.
   */
  newGame = () => {
    this.setState({ cards: this.generateCards(), guesses: 0, currentPair: [], matchedCardIndices: [], remainingTry: MAX_ERRORS });
  }

  /**
   * Render the component.
   *
   * @return HTML code of component.
   */
  render() {
    const { cards, guesses, remainingTry } = this.state;

    return (
      <div className="game">
        <div className="title">Memory animalier</div>
        <div className="content">
          <div className="memory">
            {cards.map((card, index) =>
              <Card
                card={card}
                feedback={this.getFeedbackForCard(index)}
                key={index}
                index={index}
                onClick={this.handleCardClick}
              />
            )}
          </div>
          <div className="informations">
            <button className="btnInfo" onClick={this.newGame}>Nouvelle partie</button>
            <div className="counter"> Nombre d'erreurs restantes : {remainingTry}/{MAX_ERRORS}</div>

            <GuessCount guesses={guesses} />
            <div className="endGame">
              {
                this.gameState() === GameState.WIN &&
                <div className="win">
                  Félicitation, tu as gagné en {guesses} tentatives !
            </div>
              }
              {
                this.gameState() === GameState.LOOSE &&
                <div className="loose">
                  Trop d'erreurs, tu as perdu !
          </div>
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
