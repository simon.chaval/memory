import React from 'react'
import './GuessCount.css'
import PropTypes, { InferProps } from 'prop-types'

/**
 * Function to count guesses.
 */
export function GuessCount({ guesses }: InferProps<typeof GuessCount.propTypes>) {
    return (
        <div className="guesses">
            <div className="counter"> Compteur d'essaies : {guesses}</div>
        </div>
    )
}

GuessCount.propTypes = {
    /**
     * Count the guesses.
     */
    guesses: PropTypes.number.isRequired,
}
